package com.example.vir4.config;

import com.example.vir4.domain.Privilege;
import com.example.vir4.domain.Role;
import com.example.vir4.domain.User;
import com.example.vir4.repository.PrivilegeRepository;
import com.example.vir4.repository.RoleRepository;
import com.example.vir4.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    public static final String JPG_PRIVILEGE = "JPG_PRIVILEGE";
    public static final String PNG_PRIVILEGE = "PNG_PRIVILEGE";
    public static final String GIF_PRIVILEGE = "GIF_PRIVILEGE";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    boolean alreadySetup = false;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PrivilegeRepository privilegeRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        Privilege jpgReadPrivilege = createPrivilegeIfNotFound(JPG_PRIVILEGE, "jpg");
        Privilege pngReadPrivilege = createPrivilegeIfNotFound(PNG_PRIVILEGE, "png");
        Privilege gifReadPrivilege = createPrivilegeIfNotFound(GIF_PRIVILEGE, "gif");

        Role userRole = createRoleIfNotFound(ROLE_USER);
        Role adminRole = createRoleIfNotFound(ROLE_ADMIN);

        User jpgUser = User.builder()
                .username("jpg")
                .email("jpg@jpg.jpg")
                .password(passwordEncoder.encode("jpg"))
                .roles(List.of(userRole))
                .privilages(List.of(jpgReadPrivilege))
                .build();

        User pngUser = User.builder()
                .username("png")
                .email("png@png.png")
                .password(passwordEncoder.encode("png"))
                .roles(List.of(userRole))
                .privilages(List.of(pngReadPrivilege))
                .build();

        User gifUser = User.builder()
                .username("gif")
                .email("gif@gif.gif")
                .password(passwordEncoder.encode("gif"))
                .roles(List.of(userRole))
                .privilages(List.of(gifReadPrivilege))
                .build();


        User adminUser = User.builder()
                .username("admin")
                .email("admin@admin.admin")
                .password(passwordEncoder.encode("admin"))
                .roles(List.of(adminRole))
                .privilages(List.of())
                .build();

        List<User> users = List.of(jpgUser, pngUser, gifUser,adminUser);

        userRepository.saveAll(users);

        alreadySetup = true;
    }

    @Transactional
    public Privilege createPrivilegeIfNotFound(String name, String extension) {
        Optional<Privilege> privilege = privilegeRepository.findByName(name);
        if (privilege.isPresent()) {
            return privilege.get();
        }
        Privilege newPrivilege = Privilege.builder()
                .name(name)
                .extension(extension)
                .build();
        return privilegeRepository.save(newPrivilege);
    }

    @Transactional
    public Role createRoleIfNotFound(String name) {
        Optional<Role> role = roleRepository.findByName(name);
        if (role.isPresent()) {
            return role.get();
        }

        Role newRole = Role.builder()
                .name(name)
                .build();

        return roleRepository.save(newRole);
    }
}