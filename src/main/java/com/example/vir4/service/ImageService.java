package com.example.vir4.service;

import com.example.vir4.config.SetupDataLoader;
import com.example.vir4.domain.Privilege;
import com.example.vir4.domain.User;
import com.example.vir4.dto.ImageAndExtension;
import com.example.vir4.exception.ImageNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ImageService {
    private final PrivilegeService privilegeService;
    @Value("${vir4.image.library}")
    private String imageFolder;

    public List<String> getImageNames(User user) {
        List<String> supportedExtensions = privilegeService.getSupportedExtensions();
        try (Stream<Path> walk = Files.walk(Paths.get(imageFolder))) {
            return walk.filter(p -> !Files.isDirectory(p))
                    .map(p -> p.getFileName().toString().toLowerCase())
                    .filter(fileName -> userShouldHavePrivilageToSeeImage(user, fileName, supportedExtensions))
                    .toList();
        } catch (Exception ignored) {
            throw new RuntimeException("Folder not Found");
        }
    }

    public ImageAndExtension getImageByFileName(String fileName, User user) {
        List<String> supportedExtensions = privilegeService.getSupportedExtensions();

        if (!userShouldHavePrivilageToSeeImage(user, fileName, supportedExtensions)) {
            throw new ImageNotFoundException(fileName);
        }

        String filePathName = String.format("%s/%s", imageFolder, fileName);
        String extension = getExtension(fileName);

        try {
            BufferedImage bImage = ImageIO.read(new File(filePathName));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, extension, bos);
            return new ImageAndExtension(bos.toByteArray(), extension);
        } catch (IOException e) {
            throw new ImageNotFoundException(fileName);
        }

    }

    private boolean userShouldHavePrivilageToSeeImage(User user, String fileName, List<String> supportedExtensions) {
        return fileIsImageThatWeSupport(fileName, supportedExtensions) &&
                (user.getRoles().stream().anyMatch(role -> role.getName().equals(SetupDataLoader.ROLE_ADMIN)) ||
                 user.getPrivilages().stream().map(Privilege::getExtension)
                         .anyMatch(extension -> fileExtensionEqualsExtension(fileName, extension)));
    }

    private boolean fileIsImageThatWeSupport(String fileName, List<String> supportedExtensions) {
        return supportedExtensions.stream().anyMatch(fileName::endsWith);
    }

    private boolean fileExtensionEqualsExtension(String fileName, String extension) {
        return fileName.endsWith(extension);
    }

    private String getExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index > 0) {
            return fileName.substring(index + 1);
        }
        throw new ImageNotFoundException(fileName);
    }
}
