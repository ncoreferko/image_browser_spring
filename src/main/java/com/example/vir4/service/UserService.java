package com.example.vir4.service;

import com.example.vir4.domain.User;
import com.example.vir4.exception.UserNotFoundException;
import com.example.vir4.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User getByUserName(String userName) {
        return userRepository.getUserByUsername(userName)
                .orElseThrow(() -> new UserNotFoundException(userName));
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public void save(User user) {
        userRepository.save(user);
    }

}
