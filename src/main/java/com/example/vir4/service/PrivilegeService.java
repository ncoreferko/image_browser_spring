package com.example.vir4.service;

import com.example.vir4.domain.Privilege;
import com.example.vir4.exception.PrivilegeNotFoundException;
import com.example.vir4.repository.PrivilegeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PrivilegeService {
    private final PrivilegeRepository privilegeRepository;

    public List<String> getSupportedExtensions() {
        return privilegeRepository.findAll().stream().map(Privilege::getExtension).toList();
    }

    public Privilege getPrivilegeByExtension(String extension) {
        return privilegeRepository.findPrivilegeByExtension(extension)
                .orElseThrow(() -> new PrivilegeNotFoundException(extension));

    }

}
