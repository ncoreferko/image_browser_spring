package com.example.vir4.service;


import com.example.vir4.config.SetupDataLoader;
import com.example.vir4.domain.Privilege;
import com.example.vir4.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminService {
    private final UserService userService;
    private final PrivilegeService privilegeService;

    public void changePrivilege(String userName, String extension) {
        User user = userService.getByUserName(userName);
        Privilege privilege = privilegeService.getPrivilegeByExtension(extension);

        if (user.getPrivilages().contains(privilege)) {
            user.getPrivilages().remove(privilege);
        } else {
            user.getPrivilages().add(privilege);
        }

        userService.save(user);
    }

    public String getAllNonAdminUserAndPrivilege() {
        List<String> supportedExtensions = privilegeService.getSupportedExtensions();

        return userService.getAllUser().stream()
                .filter(user -> user.getRoles().stream().noneMatch(role -> role.getName().equals(SetupDataLoader.ROLE_ADMIN)))
                .map(user -> getUserStringHTML(user, supportedExtensions))
                .collect(Collectors.joining("<br>"));
    }

    private String getUserStringHTML(User user, List<String> supportedExtensions) {
        return String.format("""
                <span>User: %s -> </span>
                %s
                """, user.getUsername(), getPrivilegeChangeStringHTML(user.getUsername(), user.getPrivilages(), supportedExtensions));
    }

    private String getPrivilegeChangeStringHTML(String username, Collection<Privilege> privileges, List<String> supportedExtensions) {

        return supportedExtensions.stream()
                .map(extension -> String.format("<a href='%s'>%s</a> ", getChangeUrl(username, extension), getExtensionString(extension, privileges.stream().anyMatch(privilege -> privilege.getExtension().equals(extension)))))
                .collect(Collectors.joining(" | "));
    }

    private String getExtensionString(String extension, boolean userHasPrivilege) {
        return String.format("%s %s privilege", userHasPrivilege ? "Remove" : "Add", extension);
    }

    private String getChangeUrl(String userName, String extension) {
        return String.format("users/privilege/%s/%s",userName, extension);
    }
}
