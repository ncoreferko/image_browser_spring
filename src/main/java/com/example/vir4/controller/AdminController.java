package com.example.vir4.controller;

import com.example.vir4.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/admin")
@RequiredArgsConstructor
class AdminController {
    private final AdminService adminService;


    @GetMapping("users")
    public String getAllUserAndPrivilege() {
        return adminService.getAllNonAdminUserAndPrivilege();
    }

    @GetMapping("users/privilege/{userName}/{extension}")
    public String changePrivilege(@PathVariable String userName, @PathVariable String extension) {
        adminService.changePrivilege(userName, extension);
        return "Succes :)";
    }
}
