package com.example.vir4.controller;

import com.example.vir4.domain.User;
import com.example.vir4.service.ImageService;
import com.example.vir4.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/image")
class ImageController {
    private final ImageService imageService;
    private final UserService userService;

    @GetMapping(produces = "text/html")
    public String getAvailableImages(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User currentUser = userService.getByUserName(userDetails.getUsername());

        return imageService.getImageNames(currentUser).stream().map(this::createLinkFromImageName).collect(Collectors.joining());
    }

    @GetMapping(value = "{fileName}")
    public ResponseEntity<byte[]> getImage(@PathVariable String fileName, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User currentUser = userService.getByUserName(userDetails.getUsername());

        var imageAndExtension = imageService.getImageByFileName(fileName, currentUser);
        MediaType contentType = getMediaType(imageAndExtension.extension());
        return ResponseEntity.ok()
                .contentType(contentType)
                .body(imageAndExtension.image());
    }
    private String createLinkFromImageName(String imageName) {
        final String link = String.format("image/%s", imageName);
        return String.format("<a href='%s'>%s</a><br>", link, imageName);
    }

    private MediaType getMediaType(String extension) {
        return switch (extension) {
            case "jpg" -> MediaType.IMAGE_JPEG;
            case "png" -> MediaType.IMAGE_PNG;
            case "gif" -> MediaType.IMAGE_GIF;
            default -> MediaType.TEXT_PLAIN;
        };
    }
}
