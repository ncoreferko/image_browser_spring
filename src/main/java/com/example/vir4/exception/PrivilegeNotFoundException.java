package com.example.vir4.exception;

public class PrivilegeNotFoundException extends RuntimeException {
    public PrivilegeNotFoundException(String extension) {
        super(String.format("Extension %s not supported", extension));
    }
}
