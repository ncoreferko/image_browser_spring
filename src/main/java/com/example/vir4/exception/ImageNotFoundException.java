package com.example.vir4.exception;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(String fileName) {
        super(String.format("Image not found on path %s", fileName));
    }
}
