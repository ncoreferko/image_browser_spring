package com.example.vir4.dto;

import java.util.Objects;

public final class ImageAndExtension {
    private final byte[] image;
    private final String extension;

    public ImageAndExtension(byte[] image, String extension) {
        this.image = image;
        this.extension = extension;
    }

    public byte[] image() {
        return image;
    }

    public String extension() {
        return extension;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (ImageAndExtension) obj;
        return Objects.equals(this.image, that.image) &&
                Objects.equals(this.extension, that.extension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(image, extension);
    }

    @Override
    public String toString() {
        return "ImageAndExtension[" +
                "image=" + image + ", " +
                "extension=" + extension + ']';
    }
}