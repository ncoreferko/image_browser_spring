package com.example.vir4.repository;

import com.example.vir4.domain.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
    Optional<Privilege> findByName(String name);
    Optional<Privilege> findPrivilegeByExtension(String extension);
}
